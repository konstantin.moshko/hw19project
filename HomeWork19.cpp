﻿#include <iostream>

using namespace std;

class Animal
{
public:
    Animal() {}

   virtual const char* voice() 
   {
       return "\n "; 
   };

   virtual ~Animal() {}
       
};

class Dog : public Animal
{
public:
    Dog() {}

    virtual const char* voice() 
    {
        return "Гав\n"; 
    };

    virtual ~Dog() {}
       
};

class Cat : public Animal
{
public:
    Cat() {}
    virtual const char* voice() 
    {
        return "Мяу\n"; 
    };

    virtual ~Cat() {}

    

   
};

class Cow : public Animal
{
public:
    Cow() {}
    virtual const char* voice() 
    {
        return "Муу\n"; 
    };

    virtual ~Cow() {}
    
};



int main()
{
    setlocale(0, "");
       
    int size;
    size = 3;
    Cat* C = new Cat;
    Cow* W = new Cow;
    Dog* D = new Dog;

    Animal** m = new Animal * [size] {C, W, D};

    for (int i = 0; i < size; i++)
    {
        cout << m[i]->voice();
        delete m[i];

    }
    
    delete[] m;
           
    return 0;
}

